from datetime import datetime
from os import environ, path, makedirs, listdir
from re import sub, DOTALL

from markdown import markdown

ENVIRONMENT: str = environ.get('ENVIRONMENT', 'development')
IS_PRODUCTION_ENV: bool = (ENVIRONMENT == 'production')

BASE_DOMAIN = "https://lloydlobo.gitlab.io/blog"
METADATA_INDEX = {'heading': "Boggled",
                  'description': "Sharing information that boggles me"}
METADATA_ABOUT = {'heading': "About | Boggled",
                  'description': "About this blog"}

RELATIVE_PATH_LEVEL = {
    0: "",  # public/  # Use for index.html, about.html, etc.
    1: "../",  # public/2023/
    2: "../../",  # public/2023/12/  # Use for any post.
}
FILENAME_ABOUT_HTML = "about.html"
FILENAME_FAVICON_ICO = "favicon.ico"
FILENAME_INDEX_HTML = "index.html"
FILENAME_RSS_XML = "RSS.xml"
FILENAME_STYLE_CSS = "style.css"

SCRIPT_DIR = path.dirname(path.dirname(__file__))
CONTENT_DIR = path.join(SCRIPT_DIR, "content")
CONTENT_POSTS_DIR = path.join(SCRIPT_DIR, "content", "posts")
PUBLIC_DIR = path.join(SCRIPT_DIR, "public")


#  localhost server seems to error if a url does not end with `.html`.
#  To solve this, append a suffix at end of each <a> url text.
#  It appends an empty string to the <a> url in production environment.
def add_html_ext_if_dev_env() -> str:
    return '' if IS_PRODUCTION_ENV else ".html"


def minify_css(data: str) -> str:
    data = sub(r'/\*[\s\S]*?\*/', '', data)
    data = sub(r'\s+', ' ', data)
    data = sub(r'\s*([:;{}])\s*', r'\1', data)
    data = sub(r'(\s+!important)', r'\1', data)
    return data.strip()


def minify_html(data: str) -> str:
    data = sub(r'<!--(.*?)-->', '', data, flags=DOTALL)
    data = sub(r'\s+', ' ', data)
    data = sub(r'>\s*<', '><', data)
    return data.strip()


def gen_rss_xml_feed(posts_array) -> str:
    item_list: str = '\n'.join([f"""\
\t\t<item>
\t\t\t<link>{BASE_DOMAIN}/{post['metadata']['relative_url']}.html</link>
\t\t\t<title>{post['metadata']['heading']}</title>
\t\t\t<pubDate>{post['metadata']['datetime_pretty']}</pubDate>
\t\t</item>\
""" for post in posts_array])

    return f"""\
<?xml version="1.0" encoding="UTF-8"?>\n
<rss version="2.0">
\t<channel>
\t\t<title>Boggled</title>
\t\t<description>Sharing information that I get boggled with</description>
\t\t<link>{BASE_DOMAIN}</link>
\t\t<language>en</language>
{item_list}
\t</channel>
</rss>\
"""


def gen_html_head(metadata, relative_level: int) -> str:
    level = RELATIVE_PATH_LEVEL[relative_level]
    has_description: bool = 'description' in metadata
    if not has_description:  # Generating for post page.
        metadata['description'] = None

    return f"""\
<head>
\t<meta charset="utf-8">
\t<meta name="viewport" content="width=device-width, initial-scale=1">
\t<meta name="title" content="{metadata['heading']}">
\t<title>{metadata['heading']}</title>\
\n\t{(f'<meta name="description" content="{metadata["description"]}">'
      ) if has_description else ''}
\t<link rel="icon" type="image/x-icon" href="{level}{FILENAME_FAVICON_ICO}">
\t<link rel="stylesheet" type="text/css" href="{level}{FILENAME_STYLE_CSS}">
\t<meta http-equiv="Content-Security-Policy" content="default-src 'self';">
</head>\
"""


def gen_html_body_header(relative_level=0) -> str:
    level = RELATIVE_PATH_LEVEL[relative_level]

    return f"""\
\t<header class="container">
\t\t<nav>
\t\t\t<div class="logo"><a href="{level}index{add_html_ext_if_dev_env()}">boggled</a></div>
\t\t\t<ul class="nav-items">
\t\t\t\t<li aria-label="About Page">
\t\t\t\t\t<a href="{level}about{add_html_ext_if_dev_env()}">About</a>
\t\t\t\t</li>
\t\t\t\t<li aria-label="Really Simple Syndication (RSS)">
\t\t\t\t\t<a href="{level}RSS.xml">RSS</a>
\t\t\t\t</li>
\t\t\t</ul>
\t\t</nav>
\t</header>\
"""


def gen_html_body_footer() -> str:
    return """\
\t<footer class="container">
\t\t
\t</footer>\
"""


def gen_post_list(post_array) -> str:
    post_list: list[str] = [f"""\
\t\t\t\t<li>
\t\t\t\t\t<h2>
\t\t\t\t\t\t<time datetime="{post['metadata']['date']}"
\t\t\t\t\t\t      title="{post['metadata']['datetime_pretty']}">
\t\t\t\t\t\t\t{post['metadata']['datetime_mdy']}
\t\t\t\t\t\t</time>
\t\t\t\t\t\t<a href="{post["metadata"]["relative_url"]}{add_html_ext_if_dev_env()}">
\t\t\t\t\t\t\t{post["metadata"]["heading"]}
\t\t\t\t\t\t</a>
\t\t\t\t\t</h2>
\t\t\t\t</li>\
""" for post in post_array]

    return '\n'.join(post_list)


def gen_html_index_page(post_array, css_path: str) -> str:
    assert (css_path == FILENAME_STYLE_CSS
            and f'Expected css path for {FILENAME_INDEX_HTML}')

    return f"""\
<!DOCTYPE html>
<html lang="en">\n
{gen_html_head(metadata=METADATA_INDEX, relative_level=0)}\n
<body>
{gen_html_body_header()}\n
\t<main>
\t\t<section class="container">
\t\t\t<h1 class="visually-hidden">So far…</h1>
\t\t\t<ul class="posts flow">
{gen_post_list(post_array=post_array)}
\t\t\t</ul>
\t\t</section>
\t</main>\n
{gen_html_body_footer()}
</body>\n
</html>\
"""


def gen_html_about_page(css_path: str) -> str:
    assert (css_path == FILENAME_STYLE_CSS
            and f'Expected css path for {FILENAME_ABOUT_HTML}')
    # \t\t\t\t<p>Code samples on this blog are licensed under MIT OR Apache-2.0.</p>

    return f"""\
<!DOCTYPE html>
<html lang="en">\n
{gen_html_head(metadata=METADATA_ABOUT, relative_level=0)}\n
<body>
{gen_html_body_header()}\n
\t<main>
\t\t<section class="container">
\t\t\t<article class="flow">
\t\t\t\t<h1>Hello, World!</h1>
\t\t\t\t<p>I am Lloyd Lobo, a programmer that occasionally writes here 
\t\t\t\t\tabout topics that completely boggles me.
\t\t\t\t</p>
\t\t\t\t<p>
\t\t\t\t\tYou can find me on <a
\t\t\t\t\t\t\thref="https://github.com/lloydlobo">GitHub</a>, 
\t\t\t\t\t<a href="https://lloydlobo.gitlab.io">website</a>, or send 
\t\t\t\t\tme <a href="mailto:hi@lloydlobo.com">an email</a>.</p>
\t\t\t</article>
\t\t</section>
\t</main>\n
{gen_html_body_footer()}
</body>\n
</html>\
"""


def gen_html_post(metadata, content) -> str:
    relative_level_to_home = 2
    input_date: datetime = datetime.strptime(metadata['date'], "%Y-%m-%d")
    metadata['datetime_pretty'], metadata['datetime_mdy'] = (
        input_date.strftime("%A, %B %d, %Y")), (input_date.strftime("%b-%d-%Y"))

    body_main_h1: str = (metadata['heading'] if metadata['heading'] is not None
                         else metadata['title'])
    body_main_h1_id = body_main_h1.replace(' ', '-')

    return f"""\
<!DOCTYPE html>
<html lang="en">\n
{gen_html_head(metadata, relative_level=2)}\n
<body>
{gen_html_body_header(relative_level=relative_level_to_home)}\n
\t<main>
\t\t<section class="container">
\t\t\t<article class="post-article flow">
\t\t\t\t<time class="post-datetime" datetime="{metadata['date']}" 
\t\t\t\t      title="{metadata['datetime_pretty']}">{metadata['datetime_mdy']}
\t\t\t\t</time>
\t\t\t\t<h1 id='{body_main_h1_id}' class="post-title">
\t\t\t\t\t<a href="#{body_main_h1_id}">{body_main_h1}</a>
\t\t\t\t</h1>
\t\t\t\t{content}
\t\t\t</article>
\t\t</section>
\t</main>\n
{gen_html_body_footer()}
</body>\n
</html>\
"""


def run() -> None:
    # --------------------------------------------------------------------------
    # READ INITIAL MARKDOWN CONTENT

    posts: list[dict[str, dict[str, str] | str]] = []

    # --------------------------------------------------------------------------
    # Scrape file name and markdown content from content folder.
    md_files = (x for x in listdir(CONTENT_POSTS_DIR) if x.endswith(".md"))
    for filename in md_files:  # 2023-12-21-foo-bar.md
        with open(file=path.join(CONTENT_POSTS_DIR, filename), mode="r",
                  encoding="utf-8") as file:
            posts.append({
                'metadata': {
                    'date': filename[:10],  # 2023-12-21
                    'title': filename[11:].strip(".md"), },  # foo-bar
                'content':  file.read(), })
    posts.reverse()  # Sort by creation most recent.

    # --------------------------------------------------------------------------
    # GENERATE BLOG

    makedirs(PUBLIC_DIR, exist_ok=True)

    for post in posts:
        # ----------------------------------------------------------------------
        # Update metadate for each post.
        p_date: str = post["metadata"]["date"]
        p_year, p_month = p_date[:4], p_date[5:7]
        post['metadata']['relative_url'] = (
            f"{p_year}/{p_month}/" f"{post['metadata']['title']}")
        p_content: list[str] = post['content'].split('\n')
        p_md_h1, p_content_rest = p_content[0], p_content[1:]
        post['metadata']['heading'] = (
            p_md_h1[2:].strip() if ((p_md_h1[0] == '#') and (
                p_md_h1[1].isspace())) else None)
        # ----------------------------------------------------------------------
        # Scrape line after title in markdown file as post description.
        buffer_desc = []
        for i, line in enumerate(p_content_rest):
            if line.strip() == "---":
                break
            if i > 3:
                print(f"{post['metadata']['title']}.md has no description")
                buffer_desc.clear()
                break
            buffer_desc.append(line)

        post['metadata']['description'] = ' '.join(
            buffer_desc).strip() if buffer_desc else None
        # ----------------------------------------------------------------------
        # Parse and generate html from markdown for each post.
        md_content_as_html_unicode: str = markdown('\n'.join(p_content_rest))

        # ----------------------------------------------------------------------
        # Link each heading to it's id to jump to it.

        def add_links_to_h2(html_content: str) -> str:
            def link_heading_to_self(match):
                h2_txt = match.group(1)
                h2_id = h2_txt.replace(' ', '-').lower()
                # print(h2_id)
                return f'<a href="#{h2_id}">{h2_txt}</a>'

            pattern = r'<h2>([^<]+)</h2>'
            return sub(pattern, (lambda match: (
                f'<h2 id="{match.group(1).replace(" ", "-").lower()}">'
                f'{link_heading_to_self(match)}'
                f'</h2>')), html_content)

        html_with_linked_headings = add_links_to_h2(md_content_as_html_unicode)
        html = gen_html_post(post['metadata'], html_with_linked_headings)

        # ----------------------------------------------------------------------
        # I/O write current post html file.
        filename_post_html = f"{post['metadata']['title']}.html"
        out_post_html_path = path.join(PUBLIC_DIR, p_year, p_month,
                                       filename_post_html)
        makedirs(path.dirname(out_post_html_path), exist_ok=True)
        if path.exists(out_post_html_path):
            print(f"Updating file {out_post_html_path}")
        with open(out_post_html_path, "w", encoding="utf-8") as post_file:
            post_file.write(minify_html(html) if IS_PRODUCTION_ENV else html)

    # ----------------------------------------------------------------------
    # I/O write index.html file.
    out_index_html_path = path.join(PUBLIC_DIR, FILENAME_INDEX_HTML)
    if path.exists(out_index_html_path):
        print(f"Updating file {out_index_html_path}")
    with open(out_index_html_path, "w", encoding="utf-8") as index_file:
        data = gen_html_index_page(post_array=posts,
                                   css_path=FILENAME_STYLE_CSS)
        index_file.write(minify_html(data) if IS_PRODUCTION_ENV else data)
    # ----------------------------------------------------------------------
    # I/O write about.html file.
    out_about_html_path = path.join(PUBLIC_DIR, FILENAME_ABOUT_HTML)
    if path.exists(out_about_html_path):
        print(f"Updating file {out_about_html_path}")
    with open(out_about_html_path, "w", encoding="utf-8") as about_file:
        data = gen_html_about_page(css_path=FILENAME_STYLE_CSS)
        about_file.write(minify_html(data) if IS_PRODUCTION_ENV else data)
    # ----------------------------------------------------------------------
    # I/O write RSS.xml file.
    out_rss_xml_path = path.join(PUBLIC_DIR, FILENAME_RSS_XML)
    if path.exists(out_rss_xml_path):
        print(f"Updating file {out_rss_xml_path}")
    with open(out_rss_xml_path, "w", encoding="utf-8") as rss_xml_file:
        rss_xml_file.write(gen_rss_xml_feed(posts_array=posts))
    # ----------------------------------------------------------------------
    # I/O read content/style.css and write it to public dir.
    src_style_css_path = path.join(CONTENT_DIR, FILENAME_STYLE_CSS)
    if not path.exists(src_style_css_path):
        print(f"Couldn't find file at {src_style_css_path}")

    dst_style_css_path = path.join(PUBLIC_DIR, FILENAME_STYLE_CSS)
    if path.exists(dst_style_css_path):
        print(f"Updating file {dst_style_css_path}")

    css_content: str | None = None
    with open(src_style_css_path, "r", encoding="utf-8") as src_css_file:
        css_content = src_css_file.read()

    if css_content is not None:
        with open(dst_style_css_path, 'w', encoding="utf-8") as dst_css_file:
            dst_css_file.write(
                minify_css(css_content) if IS_PRODUCTION_ENV else css_content)

    # ----------------------------------------------------------------------
    # I/O copy read content/favicon.ico and write it to public dir.
    src_favicon_ico_path = path.join(CONTENT_DIR, FILENAME_FAVICON_ICO)
    if not path.exists(src_favicon_ico_path):
        print(f"Couldn't find file at {src_favicon_ico_path}")

    with open(src_favicon_ico_path, "rb") as src_favicon_file:
        dst_favicon_ico_path = path.join(PUBLIC_DIR, FILENAME_FAVICON_ICO)
        if path.exists(dst_favicon_ico_path):
            print(f"Updating file {dst_favicon_ico_path}")

        with open(dst_favicon_ico_path, "wb") as dst_favicon_file:
            while True:
                data = src_favicon_file.read(1024)
                if not data:
                    break
                dst_favicon_file.write(data)


if __name__ == "__main__":
    run()
