# Creating Programs Through Smart Constraints

<b>TLDR:</b> embrace the principle of constraint and, as a consequence, craft
efficient and simple programs.

## Contents

<!-- TOC -->

* [Build Programs from Scratch and Specific to the Project](#build-programs-from-scratch-and-specific-to-the-project)
* [Avoid Over-generalization: Tailor Your Programs](#avoid-over-generalization-tailor-your-programs)
* [Refactor Code Multiple Times to Improve Its Quality and Maintainability](#refactor-code-multiple-times-to-improve-its-quality-and-maintainability)
* [Minimize Abstractions and Maintain Simplicity for Better Code Clarity](#minimize-abstractions-and-maintain-simplicity-for-better-code-clarity)
* [Bonus - Keep Website Size Small and Optimized for Performance](#bonus---keep-website-size-small-and-optimized-for-performance)

<!-- TOC -->

I recently found myself lost in the labyrinth of code while revisiting an old
program I had written. I decided to rewrite it on a whim — battling against the
complexities that seemed to multiply with every new feature being added.

The result? A program with fewer lines of code and a more readable program than
before. It was simpler and avoided most of the technical debt the previous
program was stuck with.

So, what did I learn from this?
Let's delve into the few principles behind crafting programs with smart
constraints:

## Build Programs from Scratch and Specific to the Project

Don't hesitate to reinvent the wheel. There is no overhead of third-party
dependencies if you build it yourself. It may not be feasible when creating such
a library is a giant project in and of itself.

If you own the entire architecture, you do not need to fiddle with other
programmers' library code. It's analogous to game programmers building custom
game engines for their games.

Jonathan Blows' talk
on [Preventing the Collapse of Civilization](https://youtu.be/ZSRHeXYDLko?si=XZiCfLFakYuAIncT&t=2653)
,touches on diminishing domain-level expertise as each generation builds upon
previous abstractions. Another great watch is Casey Muratoris' talk
on [The Thirty-Million Line Problem](https://youtu.be/kZRE7HIO3vk?si=g06k7SqOl7SykuiV)

## Avoid Over-generalization: Tailor Your Programs

A specific great program for a single purpose serves the higher good. For
example, UNIX or GNU/Linux coreutils like grep, awk, parallel, and sed are still
programmers' daily bread-and-butter tools. That's all you need when used with
pipes(`|`) to communicate with each other.

Make it work first, and avoid making it generic for all perceived future use
cases.

## Refactor Code Multiple Times to Improve Its Quality and Maintainability

When writing a new program, <b>rewrite it at least three times</b>. Attachment
to previous program versions may work against you. They will get better over the
next three generations if you rewrite them fearlessly.

## Minimize Abstractions and Maintain Simplicity for Better Code Clarity

Superficial abstractions are a recipe for confusion. Keep it stupid and simple,
and aim to inline functions, methods, procedures, etc, when possible.

Build programs procedurally whenever you can, and write it based on how the
compiled assembly code will execute CPU instructions.

## Bonus - Keep Website Size Small and Optimized for Performance

Aim for a 14 KB web page size that includes CSS, fonts, images, etc.

JavaScript is not necessary for most types of websites. You can improve
performance by avoiding using JavaScript from third-party libraries. The
article [Why Your Website Should Be Under 14 KB in Size](https://endtimes.dev/why-your-website-should-be-under-14kb-in-size/)
explains how a kilobyte above 14KB page size affects performance.

---

In conclusion, embracing constraints is akin to <b>teaching your code to dance
the programming "cha-cha"</b>. By doing so, programs not only _elegantly_
withstand the test of time — but also _sidestep_ those pesky, human-prone bugs
with style!

Recap of Key Points:

* Focus on writing performance-aware code.
* Tailor programs to specific user and project requirements.
* Constantly tweak programs in response to an evolving system.
* Minimize abstraction by favoring procedural programming.
